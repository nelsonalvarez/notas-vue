import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

// Bootstrap
import 'bootstrap';
import 'bootstrap'; import 'bootstrap/dist/css/bootstrap.min.css';

// Fortawesome
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

// Notificaciones en pantalla
import Snotify, {SnotifyToastConfig} from 'vue-snotify';
Vue.use(Snotify);


import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  beforeCreate() {
    Vue.$snotify = this.$snotify;
  },
  render: h => h(App)
}).$mount('#app')
