import firebase from 'firebase/app'
import firestore from 'firebase/firestore'

const config = {
    apiKey: "AIzaSyBG-pi8n4IcFLc32gKjbG8SQtTOS_J8qmU",
    authDomain: "vue-crud-neku.firebaseapp.com",
    databaseURL: "https://vue-crud-neku.firebaseio.com",
    projectId: "vue-crud-neku",
    storageBucket: "vue-crud-neku.appspot.com",
    messagingSenderId: "209654822973",
    appId: "1:209654822973:web:15200b835d4d4851949254",
    measurementId: "G-VNQ7ERRTQJ"
};

const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore()