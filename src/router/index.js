import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { transitionName: 'slide' }
  },

  {
    path: '/nueva',
    name: 'nueva',
    meta: { transitionName: 'slide' },
    component: () => import(/* webpackChunkName: "about" */ '../views/Nueva.vue')
  },

  {
    path: '/lista',
    name: 'lista',
    meta: { transitionName: 'slide' },
    component: () => import(/* webpackChunkName: "about" */ '../views/ListaTareas.vue')
  },

  {
    path: '/editar/:id',
    name: 'editar',
    meta: { transitionName: 'slide' },
    component: () => import(/* webpackChunkName: "about" */ '../views/Editar.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
