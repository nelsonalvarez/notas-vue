import Vue from 'vue'
import Vuex from 'vuex'
import db from '@/firebase/firebase'
import router from '../router/index'
// SweetAlert2
import Swal from 'sweetalert2'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    
    // Arreglo de Tareas traidas desde Firestore
    tareas: [],
    
    
    // Tarea, molde
    tarea: {
      nombre: '',
      id: '',
      estado: ''
    }

  },

  // Modifica las variables u objetos del state
  mutations: {

    // Relleno el array de tareas
    setTareas(state, tareas){
      state.tareas = tareas;
    },

    // Coloco en el state la tarea seleccionada o creada
    setTarea(state, tarea){
      state.tarea = tarea
    },

    // Elimino del array de tareas la que coincida con el ID pasado como parametro por el usuario
    eliminarTarea(state, id){
      state.tareas = state.tareas.filter( doc => {
        return doc.id != id
      } )
    }

  },

  // Funciones, tareas async o bien de procesos varios
  actions: {

    //Creo una tarea Nueva
    nuevaTarea({commit}, tarea){

      // Pongo cartel de cargando...
      Swal.fire({
        title: 'Enviando información',
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor.'
      });
      Swal.showLoading();

      // Mando a la BD
      db.collection('tareas').add({
        nombre: tarea.nombre,
        estado: tarea.estado
      })
      .then(doc => {
        
        // Cierro Cargando
        Swal.close();

       // Muestro ok       
       Vue.$snotify.success('Tarea creada Ok!', {
         timeout: 2500,
         showProgressBar: true,
         closeOnClick: true,
         pauseOnHover: true
       });


        router.push({name: 'lista'})
      })
      .catch( err => {

        // Cierro Cargando
        Swal.close();

        // Muestro Error
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Error al crear la tarea, intente mas tarde',
          confirmButtonColor: '#CC0000',
          confirmButtonText: 'Volver a Intentar',
        })
      })
    },


    // Traigo las tareas desde FIREBASE y completo el array tareas ( mediante el commit a la mutation setTarea)
    getTareas({commit}){

      // Creo el arreglo provisorio tareas (luego se los paso a la mutation como parametro)
      const tareas = []

      //Muestro cargando hasta que venga la DATA
      Swal.fire({
        title: 'Cargando información',
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor.'
      });
      Swal.showLoading();

      // Cierro Cargando

      // LLamo a FIREBASE PARA TRAER LAS TAREAS
      db.collection('tareas').get()
      .then(snapshot => {

        snapshot.forEach( doc => {
          
          // Creo el objeto provisorio tarea con los parametros que necesita y hago push al array
          let tarea = doc.data();
          tarea.id = doc.id;
          tareas.push(tarea)

           // Cierro Cargando ok
          Swal.close();

        } )
        
      })
      .catch( err => {
        // Cierro Cargando
        Swal.close();
        // Muestro Error
        
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Error al cargar las tareas, intente mas tarde...',
          confirmButtonColor: '#CC0000',
          confirmButtonText: 'Volver a Intentar',
        })
      })

      // Cuando termino con el array tarea, hago commit al state
      commit('setTareas', tareas)

     
    },


    // Traigo una Tarea en especifico, por su ID
    getTarea({commit}, id){


      Swal.fire({
        title: 'Enviando información',
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor.'
      });
      Swal.showLoading();


      db.collection('tareas').doc(id).get()
      .then(doc => {
        let tarea = doc.data();
        tarea.id = doc.id
        commit('setTarea', tarea)

         // Cierro Cargando ok
         Swal.close();

      })
    },

    // Edito la tarea seleccionada....
    editarTarea({commit}, tarea){

      // Pongo Cargando Mientras se actualiza la tarea
      Swal.fire({
        title: 'Enviando información',
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor.'
      });
      Swal.showLoading();

      
      db.collection('tareas').doc(tarea.id).update({
        nombre: tarea.nombre,
        estado: tarea.estado
      })
      .then(()=>{
        commit('setTarea', tarea);
        // Cierro Cargando
        Swal.close();

        // Muestro ok
        
        Vue.$snotify.info('Tarea Editada', {
          timeout: 2500,
          showProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true
        });

        router.push({name: 'lista'})
      })
      .catch( err => {
         // Cierro Cargando
         Swal.close();

         // Muestro Error
         Swal.fire({
           icon: 'error',
           title: 'Error',
           text: 'Error al editar la tarea, intente mas tarde...',
           confirmButtonColor: '#CC0000',
           confirmButtonText: 'Volver a Intentar',
         })
      })
    },

    //Elimino tareas
    eliminarTarea({commit}, id){

      // Pongo Cargando Mientras se actualiza la tarea
      Swal.fire({
        title: 'Enviando información',
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor.'
      });
      Swal.showLoading();


      db.collection('tareas').doc(id).delete()
        .then( () => {
          commit('eliminarTarea', id) 
          
          // Cierro Cargando
          Swal.close();

          // Muestro ok
          
          Vue.$snotify.info('Tarea Eliminada !', {
          timeout: 2500,
          showProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
        });
        
        })

        .catch( err => {
          // Cierro Cargando
          Swal.close();
 
          // Muestro Error
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Error al eliminar la tarea, intente mas tarde...',
            confirmButtonColor: '#CC0000',
            confirmButtonText: 'Volver a Intentar',
          })
       })
        
    },


    salir() {
      Swal.fire({
        title: 'Salir',
        text: "¿Esta seguro que desa salir del sistema?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Salir',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        
      })
    }





  },

  
})
