# vue-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### GIT
git clone https://gitlab.com/nelsonalvarez/notas-vue.git
cd notas-vue
git add .
git commit -m "Commit Inicial"
git push origin master

